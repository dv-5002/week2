package Test.problem1;

public class Main {
    public static void main(String[] args) {
	    Student s1 = new Student("Kull","Thana","SE5002",50);
	    s1.printInfo();
	    s1.printScore();

	    Teacher t1 = new Teacher("John","Doe");
	    t1.printInfo();
	    t1.printSpecialty();

	    EnglishTeacher e1 = new EnglishTeacher("Joseph","Deck");
	    e1.printInfo();
	    e1.printSpecialty();
    }
}
