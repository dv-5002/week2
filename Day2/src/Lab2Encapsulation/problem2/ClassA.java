package Lab2Encapsulation.problem2;

public class ClassA {
    private int i;

    public ClassA() {
        this.i = 10;
    }

    public ClassA(int num) {
        this.i = num;
    }
}
