package Lab1AbstractClassAndInterface.Example3;

public interface IMeasureable {
    public double showData();
}
