package Lab1AbstractClassAndInterface.problem5;

public class DriveThrough extends Restaurant {
    private double discount;

    @Override
    public double calculateNetPrice(int price) {
        randomDiscount();
        return price - (price * discount);
    }

    private void randomDiscount() {
        double chance = Math.floor(Math.random() * 100);
        if (chance < 20) {
            this.discount = 0.1;
        } else if (chance < 30) {
            this.discount = 0.7;
        } else {
            this.discount = 0.05;
        }
    }
}
