package Lab1AbstractClassAndInterface.problem3_5;

public interface Calculate {
    public double getTotalLength();

    public double getArea();
}
