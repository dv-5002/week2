package Lab2Polymorphism.problem3;

public class Subtractor extends MathObject {
    public Subtractor(int firstValue, int secondValue) {
        super(firstValue, secondValue);
    }

    public int compute() {
        return firstValue - secondValue;
    }
}
