package Lab1AbstractClassAndInterface.problem4;

public class Pikachu extends Pokemon {
    @Override
    public void attack(int num) {
        System.out.println("Release voltage " + num + " volt");
    }

    @Override
    public void move(int distance) {
        System.out.println("Jump " + (int) Math.ceil(distance / 5.0) + " times");
    }

    @Override
    public void specialMove() {
        double chance = Math.floor(Math.random() * 100);
        if (chance < 30) {
            System.out.println("Success to paralyze the enemy");
        } else {
            System.out.println("Fail to paralyze the enemy");
        }
    }
}
