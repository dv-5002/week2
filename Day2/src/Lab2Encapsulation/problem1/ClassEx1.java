package Lab2Encapsulation.problem1;

public class ClassEx1 {
    public static void main(String args[]) {
        System.out.println("A Simple class with 2 objects-obA and obB");
        ClassA obA = new ClassA();
        ClassA obB = new ClassA(10);
        System.out.println("obA.i = " + obA.i);
        System.out.println("obB.i = " + obB.i);
    }
}
