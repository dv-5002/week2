package Lab1AbstractClassAndInterface.problem2;

public abstract class Instrument {
    protected String name;
    protected int numOfString;


    public Instrument(String name, int numOfString) {
        this.name = name;
        this.numOfString = numOfString;
    }


    public abstract void play();

    public String getName() {
        return name;
    }

    public int getNumOfString() {
        return numOfString;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumOfString(int numOfString) {
        this.numOfString = numOfString;
    }
}
