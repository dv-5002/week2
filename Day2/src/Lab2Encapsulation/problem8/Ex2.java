package Lab2Encapsulation.problem8;

import Lab2Encapsulation.problem7.Point;

public class Ex2 {
  public static void main(String[] args) {
    Line line1 = new Line(new Point(0, 0), new Point(3, 3));
    System.out.println(line1.print());
    System.out.println("The length is "+line1.length());

    Line line2 = new Line(new Point(0, 2), new Point(2, 0));
    System.out.println(line2.print());
    System.out.println("The length is "+line2.length());
  }
}
