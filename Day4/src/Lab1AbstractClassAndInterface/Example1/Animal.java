package Lab1AbstractClassAndInterface.Example1;

public class Animal {
    private int leg;
    private String name;

    public Animal(int leg, String name) {
        this.leg = leg;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getLeg() {
        return leg;
    }

    public void setLeg(int leg) {
        this.leg = leg;
    }
}
