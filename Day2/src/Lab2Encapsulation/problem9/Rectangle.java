package Lab2Encapsulation.problem9;

import Lab2Encapsulation.problem8.Line;

public class Rectangle {
    private Line[] lines;

    public Rectangle(Line line1, Line line2, Line line3, Line line4) {
        lines = new Line[4];
        lines[0] = line1;
        lines[1] = line2;
        lines[2] = line3;
        lines[3] = line4;
    }

    public void print() {
        System.out.print("The 1st point is " + lines[0].printRect());
        System.out.print("The 2nd point is " + lines[1].printRect());
        System.out.print("The 3rd point is " + lines[2].printRect());
        System.out.println("The 4th point is " + lines[3].printRect());

    }
}
