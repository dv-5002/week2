package Lab1AbstractClassAndInterface.Example1;

public class Fish extends Animal {
    public Fish() {
        super(0, "Fish");
    }

    public void canMove(){
        System.out.println(this.getName()+" can run with "+this.getLeg()+" legs.");
    }

    public void canEat(){
        System.out.println("Fish can eat spider");
    }
}
