package Lab2Polymorphism.problem1;

public class Multiply {
    public void mul(int a, int b) {
        System.out.println("Product of two = " + (a * b));
    }

    public void mul(int a, int b, int c) {
        System.out.println("Product of three = " + (a * b * c));
    }

    public void mul(double a, double b) {
        System.out.println("Product of two = " + (a * b));
    }
}
