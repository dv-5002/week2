package Test.problem1;

public class EnglishTeacher extends Person implements InterfacePerson{
    private String fname;
    private String lname;
    private String ID;
    private String teachingCourse = "English";

    public EnglishTeacher(String fname, String lname) {
        this.fname = fname;
        this.lname = lname;
    }

    public void printInfo(){
        System.out.println("My name is "+fname+" "+lname);
    }

    public void printSpecialty(){
        System.out.println("I can teach "+teachingCourse);
    }
}
