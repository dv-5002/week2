package Lab1AbstractClassAndInterface.Example2;

public class Main {
    public static void main(String args[]){
        Item f = new Food();
        Item c = new Clothing();
        Item b = new Book();

        double tax1=Item.caltax(f);
        double tax2=Item.caltax(c);
        double tax3=Item.caltax(b);

        System.out.println(f.getName()+" Price: "+f.getPrice()+" Tax = "+tax1);
        System.out.println(c.getName()+" Price: "+c.getPrice()+" Tax = "+tax2);
        System.out.println(b.getName()+" Price: "+b.getPrice()+" Tax = "+tax3);
    }
}
