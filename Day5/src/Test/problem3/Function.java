package Test.problem3;

public abstract class Function{
    protected double money;

    public abstract double deposit(double money);
    public abstract double withdraw(double money);
}
