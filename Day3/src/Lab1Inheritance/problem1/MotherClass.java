package Lab1Inheritance.problem1;

public class MotherClass {
    private String text = "Hello World";

    public void sayHello() {
        System.out.println(text);
    }
}
