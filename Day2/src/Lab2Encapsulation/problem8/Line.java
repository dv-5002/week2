package Lab2Encapsulation.problem8;

import Lab2Encapsulation.problem7.Point;

public class Line {
  private Point firstPoint;
  private Point secondPoint;

  public Line(Point firstPoint, Point secondPoint) {
    this.firstPoint = firstPoint;
    this.secondPoint = secondPoint;
  }

  public double length() {
    return Math.sqrt(
        Math.pow(this.secondPoint.getXCoordinate() - this.firstPoint.getXCoordinate(), 2) +
        Math.pow(this.secondPoint.getYCoordinate() - this.firstPoint.getYCoordinate(), 2)
    );
  }

  public String print() {
    String print=String.format("The first point is (%f,%f)",firstPoint.getXCoordinate(),firstPoint.getYCoordinate())+String.format("The second point is (%f,%f)",secondPoint.getXCoordinate(),secondPoint.getYCoordinate());
    return print;
  }

  public String printRect() {
    return String.format("(%f,%f)",firstPoint.getXCoordinate(),firstPoint.getYCoordinate());
  }
}
