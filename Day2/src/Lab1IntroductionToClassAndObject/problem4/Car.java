package Lab1IntroductionToClassAndObject.problem4;

public class Car {
    String manufacturer;
    String color;
    double speed;

    public void brake(double v){
        speed-=v;
    }

    public void accelerate(double v){
        speed+=v;
    }
}
