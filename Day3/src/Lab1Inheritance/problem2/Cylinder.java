package Lab1Inheritance.problem2;

public class Cylinder extends Circle {
    private double height;

    public Cylinder() {
        this.height = 1.0;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double calVolume() {
        return calArea() * height;
    }

    public double calSurfaceArea() {
        return (calCircumference() * height) + (2 * calArea());
    }
}
