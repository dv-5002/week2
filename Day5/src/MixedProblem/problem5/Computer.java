package MixedProblem.problem5;

public class Computer {
    private CPU myCPU;
    private HDD myHDD;
    private RAM myRAM;

    public void displaySpecs() {
        System.out.println("This computer is " + myCPU.getCPUspeed() + " with RAM " + myRAM.getRAM() + " with HDD " + myHDD.getHDDcapacity() + ".");
    }
}
