package MixedProblem.problem5;

public class CPU {
    private double CPUspeed;

    public void setCPUspeed(double CPUspeed) {
        this.CPUspeed = CPUspeed;
    }

    public double getCPUspeed() {
        return CPUspeed;
    }

    public CPU(double CPUspeed) {
        this.CPUspeed = CPUspeed;
    }
}
