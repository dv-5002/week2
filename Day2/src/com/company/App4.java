package com.company;

import java.util.Scanner;

public class App4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Input number:");
        int num = scan.nextInt();
        System.out.println(factorial(num));
    }

    public static int factorial(int num){
        for(int i = num-1; i > 1; i--){
            num *= i;
        }
        return num;
    }
}
