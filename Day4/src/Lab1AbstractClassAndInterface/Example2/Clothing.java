package Lab1AbstractClassAndInterface.Example2;

public class Clothing extends Item {
    private String size;

    public Clothing() {
        super("T-Shirt", 399, "Fur");
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
