package Lab2Polymorphism.example1;

public class Main {
    public static void main(String args[]){
        Calculation cal=new Calculation();
        cal.sum(5,5);
        cal.sum(2.5,3.5);
        cal.sum("Don't","Know");
        cal.sum(true,false);
    }
}
