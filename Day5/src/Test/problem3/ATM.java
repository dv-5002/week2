package Test.problem3;

public class ATM {
    public static void main(String args[]){
        ATMSystem d1 = new ATMSystem();
        ATMSystem w1 = new ATMSystem();
        double initialMoney = 1000;

        System.out.println("My name is AJ Yam");

        System.out.println("Deposit money");
        initialMoney += d1.deposit(500);
        System.out.println(initialMoney);

        System.out.println("Withdraw money");
        initialMoney += w1.withdraw(100);
        System.out.println(initialMoney);

        System.out.println("Current money");
        System.out.println(initialMoney);
    }
}
