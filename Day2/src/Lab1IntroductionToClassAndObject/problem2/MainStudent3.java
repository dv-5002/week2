package Lab1IntroductionToClassAndObject.problem2;

public class MainStudent3 {
  public static void main(String[] args) {
    Student apisit = new Student();
    apisit.name = "Apisit";
    apisit.studentID = "4921362";
    apisit.score = 10;

    Student thaksin = new Student();
    thaksin.name = "Thaksin";
    thaksin.studentID = "402561";
    thaksin.score = 56;

    Student samak = new Student();
    samak.name = "Samak";
    samak.studentID = "489653";
    samak.score = 28;

    System.out.println(apisit.compareScore(thaksin));
    System.out.println(samak.compareScore(thaksin));
  }
}
