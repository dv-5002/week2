package Lab1AbstractClassAndInterface.problem5;

public abstract class Restaurant {
    private int ingredient;

    public void sellProduct() {
        System.out.println("Sell product");
    }

    public void orderIngredient(int request) {
        this.ingredient += request;
    }

    public void checkInventory() {
        if (this.ingredient == 0) {
            System.out.println("Out of stock");
        } else {
            System.out.println("Ingredient: " + ingredient);
        }
    }

    public abstract double calculateNetPrice(int price);
}
