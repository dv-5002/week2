package Lab2Encapsulation.problem1;

public class ClassA {
    public int i;

    public ClassA() {
        this.i = 10;
    }

    public ClassA(int num) {
        this.i = num;
    }
}
