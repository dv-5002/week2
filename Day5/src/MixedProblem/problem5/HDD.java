package MixedProblem.problem5;

public class HDD {
    private double HDDcapacity;

    public void setHDDcapacity(double HDDcapacity) {
        this.HDDcapacity = HDDcapacity;
    }

    public double getHDDcapacity() {
        return HDDcapacity;
    }

    public HDD(double HDDcapacity) {
        this.HDDcapacity = HDDcapacity;
    }
}
