package MixedProblem.problem2;

public interface InterfaceShape {
    public double calArea();
    public double calCircumference();
}
