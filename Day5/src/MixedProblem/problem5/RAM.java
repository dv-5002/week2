package MixedProblem.problem5;

public class RAM {
    private double RAM;

    public void setRAM(double RAM) {
        this.RAM = RAM;
    }

    public double getRAM() {
        return RAM;
    }

    public RAM(double RAM) {
        this.RAM = RAM;
    }
}
