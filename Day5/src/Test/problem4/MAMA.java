package Test.problem4;

public class MAMA extends Warehouse implements InterfaceWarehouse {
    public MAMA() {
        super("MAMA", "M00", 1000, 16);
    }

    public void sayInfo(){
        System.out.println("This "+getName()+" have "+getNumOfItem()+" in warehouse and is have price: "+getPrice());
    }
}
