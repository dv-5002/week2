package Lab1AbstractClassAndInterface.Example2;

public class Item {
    private String name;
    private double price;
    private String description;

    public Item(String name, double price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static double caltax(Item item) {
        if (item instanceof Food) {
            return item.getPrice() * 0.07;
        } else if (item instanceof Clothing) {
            return item.getPrice() * 0.05;
        } else if (item instanceof Book){
            return item.getPrice() * 0.025;
        }
        return -1;
    }
}
