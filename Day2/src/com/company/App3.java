package com.company;

import java.util.Scanner;

public class App3 {
    public static void main(String[] args) {
        int num;
        Scanner scan = new Scanner(System.in);
        App3 e = new App3();
        System.out.println("Input number:");
        num = scan.nextInt();
        System.out.println( num+" "+ e.IsPrime(num));
    }

    public boolean IsPrime(int num) {
        int i;
        for (i = 2; i < num; i++) {
            if (num % i == 0) {
                return true;
            }
        }
        return false;
    }
}
