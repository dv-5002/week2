package com.company;

import java.util.Scanner;

public class App2 {
    public static void main(String[] args) {
        Scanner profile = new Scanner(System.in);
        System.out.println("Enter firstname: ");
        String name=profile.next();
        System.out.println("Enter lastname: ");
        String lastName=profile.next();
        System.out.println("Enter age: ");
        int age=profile.nextInt();
        System.out.println("Enter birthday: ");
        String date=profile.next();
        System.out.println("Enter your school: ");
        String school=profile.next();
        System.out.println("Your name is "+name+" "+lastName+"\n"+"Age "+age+" Years old\n"+"Your birthday is "+date+"\n"+"Your school is "+school);
    }
}