package Lab2Encapsulation.problem9;

import Lab2Encapsulation.problem7.Point;
import Lab2Encapsulation.problem8.Line;

public class Ex3 {
    public static void main(String[] args) {
        Line line1 = new Line(new Point(0, 0), new Point(2, 0));
        Line line2 = new Line(new Point(2, 0), new Point(2, 2));
        Line line3 = new Line(new Point(2, 2), new Point(0, 2));
        Line line4 = new Line(new Point(0, 2), new Point(0, 0));

        Rectangle rect = new Rectangle(line1, line2, line3, line4);
        rect.print();
        System.out.println("The area is " + rect);
        System.out.println("The total length is " + rect);

        Line line5 = new Line(new Point(0, 2), new Point(0, 3));
        Rectangle rect2 = new Rectangle(line1, line2, line3, line4);
    }
}
