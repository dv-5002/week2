package Lab1AbstractClassAndInterface.Example1;

public class Spider extends Animal{
    public Spider() {
        super(8, "Spider");
    }

    public void canMove(){
        System.out.println(this.getName()+" can run with "+this.getLeg()+" legs.");
    }

    public void canEat(){
        System.out.println("Spider can eat cat");
    }
}
