package Lab1AbstractClassAndInterface.Example1;

public class Cat extends Animal{

    public Cat() {
        super(4, "Cat");
    }

    public void canMove(){
        System.out.println(this.getName()+" can run with "+this.getLeg()+" legs.");
    }

    public void canEat(){
        System.out.println("Cat can eat fish");
    }
}
