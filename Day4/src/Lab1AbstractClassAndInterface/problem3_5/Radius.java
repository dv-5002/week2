package Lab1AbstractClassAndInterface.problem3_5;

public interface Radius {
    public double getRadius();
}
