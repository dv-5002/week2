package Lab2Polymorphism.problem3;

public class Modder extends MathObject{
    public Modder(int firstValue, int secondValue) {
        super(firstValue, secondValue);
    }

    public int compute() {
        return firstValue % secondValue;
    }
}
