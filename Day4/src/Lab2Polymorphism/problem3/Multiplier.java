package Lab2Polymorphism.problem3;

public class Multiplier extends MathObject {
    public Multiplier(int firstValue, int secondValue) {
        super(firstValue, secondValue);
    }

    public int compute() {
        return firstValue * secondValue;
    }
}
