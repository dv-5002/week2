package Lab1Inheritance.problem3;

public class Bicycle {
    protected double speed;

    public Bicycle() {
        this.speed = 0;
    }

    public Bicycle(double speed) {
        this.speed = speed;
    }

    public void speedUp() {
        if (this.speed + 5 <= 99) {
            this.speed += 5;
            System.out.println("Current speed: " + speed);
        } else {
            System.out.println("Speed cannot exceed 99 km/h");
        }
    }

    public void Break() {
        if (this.speed - 5 < 0) {
            this.speed = 0;
        } else {
            this.speed -= 5;
            System.out.println("Current speed: " + speed);
        }
    }
}
