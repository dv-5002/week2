package Lab1Inheritance.problem2;

public class Circle {
    protected double radius;

    public Circle() {
        this.radius = 1.0;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double calArea() {
        return Math.PI * radius * radius;
    }

    public double calCircumference() {
        return 2 * Math.PI * radius;
    }
}
