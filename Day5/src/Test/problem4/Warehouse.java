package Test.problem4;

public abstract class Warehouse {
    private String name;
    private String ID;
    private int numOfItem;
    private double price;

    public Warehouse(String name, String ID, int numOfItem, double price) {
        this.name = name;
        this.ID = ID;
        this.numOfItem = numOfItem;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getNumOfItem() {
        return numOfItem;
    }

    public void setNumOfItem(int numOfItem) {
        this.numOfItem = numOfItem;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public abstract void sayInfo();

    public int withdraw(String name, int numOfItem){
        this.numOfItem-=numOfItem;
        return this.numOfItem;
    }

    public int currentItem(){
        return numOfItem;
    }
}
