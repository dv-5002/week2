package Lab2Encapsulation.problem6;

public class Main {
  public static void main(String[] args) {
    Employee employee = new Employee();
    employee.setMonthlySalary(150000);
    employee.setAmountOfWorkingDays(730);

    System.out.println(Employee.calculateTax(employee));

    Employee.improveJobLevel(employee);

    System.out.println(employee.getJobLevel());
  }
}
