package Lab1AbstractClassAndInterface.problem3_5;

public interface HeightWidth {
    public double getHeight();

    public double getWidth();
}
