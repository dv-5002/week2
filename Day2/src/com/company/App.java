package com.company;

// Area of circle
public class App {
    public static void main(String[] args) {
//        double area = 0;
//        area = Math.PI * Math.pow(5, 2);
//        System.out.println("The area is " + area);
//        area = Math.PI * Math.pow(7, 2);
//        System.out.println("The area is " + area);

        double area = 0;
        area = getCircleArea(5);
        System.out.println("The area is " + area);
    }

    private static double getCircleArea(double radias) {
        return Math.PI * Math.pow(radias, 2);
    }
}
