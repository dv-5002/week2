package Lab2Polymorphism.example1;

public class Calculation {
    public void sum(int num1, int num2) {
        System.out.println("int int");
        System.out.println(num1 + num2);
    }

    public void sum(double num1, double num2) {
        System.out.println("double double");
        System.out.println(num1 + num2);
    }

    public void sum(String text1, String text2) {
        System.out.println("String String");
        System.out.println(text1 + text2);
    }

    public void sum(boolean value1, boolean value2) {
        System.out.println("String String");
        System.out.println("" + (value1 || value2));
    }
}
