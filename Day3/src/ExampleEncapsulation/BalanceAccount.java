package ExampleEncapsulation;

public class BalanceAccount {
    private String id;
    private double money;

    public BalanceAccount() {
        this.money = 100;
    }

    public BalanceAccount(String id, double money) {
        this.id = id;
        this.money = money;
    }

    public void deposit(double amount) {
        this.money += amount;
        System.out.println("Deposit: " + amount);
    }

    public void withdraw(double amount) {
        this.money -= amount;
        System.out.println("Withdraw: " + amount);
    }

    public void viewBalance(double amount) {
        System.out.println("Balance: " + money);
    }
}
