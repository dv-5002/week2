package Lab1Inheritance.problem4;

public class Main {
    public static void main(String[] args) {
        MountainBike mountainBike = new MountainBike();
        mountainBike.speedUp(5);

        mountainBike.setGear(1);
        mountainBike.speedUp(10);

        mountainBike.Break();
    }
}
