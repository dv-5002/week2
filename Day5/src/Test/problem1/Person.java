package Test.problem1;

public abstract class Person {
    protected String fname;
    protected String lname;
    protected String ID;
    protected String teachingCourse;

    public abstract void printInfo();
}
