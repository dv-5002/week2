package Test.problem4;

public class WAIWAI extends Warehouse implements InterfaceWarehouse{
    public WAIWAI() {
        super("WAIWAI", "W00", 1000, 20);
    }

    public void sayInfo(){
        System.out.println("This "+getName()+" have "+getNumOfItem()+" in warehouse and is have price: "+getPrice());
    }
}
