package MixedProblem.problem2;

public class Rectangle extends Shape implements InterfaceShape{
    private String name = "Rectangle";

    private double width;
    private double height;

    public Rectangle() {
        this(0.0, 0.0);
    }

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double calArea() {
        return width * height;
    }

    public double calCircumference() {
        return 2.0 * width + 2.0 * height;
    }
}
