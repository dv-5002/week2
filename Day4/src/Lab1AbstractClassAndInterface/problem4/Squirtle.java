package Lab1AbstractClassAndInterface.problem4;

public class Squirtle extends Pokemon {
    @Override
    public void attack(int num) {
        System.out.println("Release water " + num + " liters");
    }

    @Override
    public void move(int distance) {
        System.out.println("Swim " + (int) Math.ceil(distance / 3.0) + " times");
    }

    @Override
    public void specialMove() {
        double chance = Math.floor(Math.random() * 100);
        if (chance < 10) {
            System.out.println("Success to deflect attack from enemy");
        } else {
            System.out.println("Fail to deflect attack from enemy");
        }
    }
}
