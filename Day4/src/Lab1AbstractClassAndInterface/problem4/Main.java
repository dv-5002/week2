package Lab1AbstractClassAndInterface.problem4;

public class Main {
    public static void main(String[] args) {
        Pokemon pikachu = new Pikachu();
        pikachu.attack(5);
        pikachu.move(13);
        pikachu.specialMove();

        System.out.println();

        Pokemon charmander = new Charmander();
        charmander.attack(5);
        charmander.move(13);
        charmander.specialMove();

        System.out.println();

        Pokemon squirtle = new Squirtle();
        squirtle.attack(5);
        squirtle.move(13);
        squirtle.specialMove();
    }
}
