package Lab1IntroductionToClassAndObject.problem5;

public class Rectangle {
  double coordinateX;
  double coordinateY;
  double width;
  double height;

  public Rectangle(double coordinateX, double coordinateY, double width, double height) {
    this.coordinateX = coordinateX;
    this.coordinateY = coordinateY;
    this.width = width;
    this.height = height;
  }

  public double calArea() {
    return width * height;
  }

  public double[][] calCoordinates() {
    return new double[][]{
            {this.coordinateX, this.coordinateY},
            {this.coordinateX, this.coordinateY + height},
            {this.coordinateX + width, this.coordinateY},
            {this.coordinateX + width, this.coordinateY + height}
    };
  }
}
