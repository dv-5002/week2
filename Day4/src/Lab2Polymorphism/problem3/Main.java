package Lab2Polymorphism.problem3;

public class Main {
    public static void main(String args[]) {
        int num1 = 15;
        int num2 = 75;
        MathObject operatorList[] = new MathObject[4];
        operatorList[0] = new Adder(num1, num2);
        operatorList[1] = new Subtractor(num1, num2);
        operatorList[2] = new Multiplier(num1, num2);
        operatorList[3] = new Modder(num1, num2);

        for (int i = 0; i < operatorList.length; i++) {
            operatorList[i].compute();
            System.out.print(operatorList[i].compute());
        }
    }
}
