package Lab1AbstractClassAndInterface.problem2;

public class Guitar extends Instrument {
    public Guitar(){
        super("Guitar",6);
    }

    @Override
    public void play(){
        System.out.println(this.getName()+" with "+this.getNumOfString()+" strings");
    }
}
