package Lab2Polymorphism.problem2;

public class Person {
    public void hit(Animal animal) {
        System.out.println(animal.roar());
    }

    public static void main(String args[]) {
        Animal dog = new Dog();
        Person mintra = new Person();
        System.out.println("First case");
        mintra.hit(dog);
        System.out.println("Second case");
        dog = new Cat();
        mintra.hit(dog);
    }
}
