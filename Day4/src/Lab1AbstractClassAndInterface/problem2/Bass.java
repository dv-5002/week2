package Lab1AbstractClassAndInterface.problem2;

public class Bass extends Instrument{
    public Bass(){
        super("Bass",4);
    }
    @Override
    public void play(){
        System.out.println(this.getName()+" with "+this.getNumOfString()+" strings");
    }
}
