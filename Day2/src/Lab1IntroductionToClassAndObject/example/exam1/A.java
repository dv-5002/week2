package Lab1IntroductionToClassAndObject.example.exam1;

public class A {
    public String str="Hello World";
    private String str2="Not Hello World";

    public void greet(){
        System.out.println(str);
    }

    public void greet2(){
        System.out.println(str2);
    }

    private String prepareText(){
        return str+str2;
    }

    public void greet3(){
        System.out.println(str2);
    }
}
