package Lab2Encapsulation.problem5;

public class Course {
  private String id;
  private String name;
  private int credit;
  private StudentGrade[] studentGrades;

  public Course() {
    studentGrades = new StudentGrade[10];
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCredit() {
    return credit;
  }

  public void setCredit(int credit) {
    this.credit = credit;
  }

  public StudentGrade[] getStudentGrades() {
    return studentGrades;
  }

  public void setStudentGrades(StudentGrade[] studentGrades) {
    this.studentGrades = studentGrades;
  }
}
