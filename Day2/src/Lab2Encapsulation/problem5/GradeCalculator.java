package Lab2Encapsulation.problem5;

import java.util.List;

public class GradeCalculator {
    public static double calculateAverage(List<Course> courses) {
        double sum = 0;
        double totalCredit = 0;
        for (int i = 0; i < courses.size(); i++) {
            int credit = courses.get(i).getCredit();
            totalCredit += courses.get(i).getStudentGrades().length * credit;
            double gradeSum = 0;
            for (int j = 0; j < courses.get(i).getStudentGrades().length; j++) {
                gradeSum += convertGradeToInteger(courses.get(i).getStudentGrades()[j].getGrade());
            }
            sum += gradeSum * credit;
        }
        return sum / totalCredit;
    }

    private static double convertGradeToInteger(String grade) {
        switch (grade) {
            case "A":
                return 4.00;

            case "B+":
                return 3.50;

            case "B":
                return 3.00;

            case "C+":
                return 2.50;

            case "C":
                return 2.00;

            case "D+":
                return 1.50;

            case "D":
                return 1.00;

            case "F":
                return 0.00;

            default:
                return -1.00;
        }
    }
}
