package MixedProblem.problem2;

public abstract class Shape {
    protected double height;
    protected double width;
    protected double radius;
}
