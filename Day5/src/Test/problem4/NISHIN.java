package Test.problem4;

public class NISHIN extends Warehouse implements InterfaceWarehouse{
    public NISHIN() {
        super("NISHIN", "N00", 1000, 25);
    }

    public void sayInfo(){
        System.out.println("This "+getName()+" have "+getNumOfItem()+" in warehouse and is have price: "+getPrice());
    }
}
