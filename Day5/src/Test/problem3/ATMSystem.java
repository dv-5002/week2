package Test.problem3;

public class ATMSystem extends Function implements InterfaceATM{
    private double money;

    @Override
    public double deposit(double money) {
        this.money += money;
        return this.money;
    }

    @Override
    public double withdraw(double money) {
        this.money -= money;
        return this.money;
    }
}
