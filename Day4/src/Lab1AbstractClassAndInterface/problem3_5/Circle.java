package Lab1AbstractClassAndInterface.problem3_5;

public class Circle extends Shape implements Radius {
    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getTotalLength() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double getArea() {
        return Math.PI * this.radius * this.radius;
    }

    @Override
    public double getRadius() {
        return this.radius;
    }
}
