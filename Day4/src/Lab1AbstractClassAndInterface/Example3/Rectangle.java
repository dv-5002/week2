package Lab1AbstractClassAndInterface.Example3;

public class Rectangle implements IMeasureable {
    private double width = 5.0;
    private double height = 6.5;

    public double showData() {
        return width * height;
    }

    public void calData(){
        System.out.println("Hey Hey");
    }
}
