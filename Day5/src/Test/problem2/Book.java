package Test.problem2;

public class Book extends Library implements InterfaceBook{
    private int numOfpage;
    private String color;
    private String character;

    public Book(int numOfpage, String color) {
        this.numOfpage = numOfpage;
        this.color = color;
    }

    public int getNumOfpage() {
        return numOfpage;
    }

    public void setNumOfpage(int numOfpage) {
        this.numOfpage = numOfpage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public void read(int numOfpage){
        System.out.println("Read book at page "+numOfpage);
    }
}
