package MixedProblem.problem1;

public class Student extends Person{
    private String text1;
    private String text2;
    private int age;

    public Student(String text1, String text2,int age){
        this.text1=text1;
        this.text2=text2;
        this.age=age;
    }

    public String SayName(){
        return "My name is "+this.text1+" "+this.text2+". "+"I am "+this.age+" years old";
    }
}
