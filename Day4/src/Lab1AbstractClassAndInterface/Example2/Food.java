package Lab1AbstractClassAndInterface.Example2;

public class Food extends Item {
    private double calories;

    public Food() {
        super("Omelet", 500, "So bad");
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }
}
