package Lab1Inheritance.problem2;

public class Main {
    public static void main(String[] args) {
        Cylinder cylinder = new Cylinder(1.0, 1.0);
        System.out.println(cylinder.calVolume());
        System.out.println(cylinder.calSurfaceArea());
    }
}
