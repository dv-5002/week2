package Lab2Polymorphism.problem3;

public class Adder extends MathObject {
    public Adder(int firstValue, int secondValue) {
        super(firstValue, secondValue);
    }

    public int compute() {
        return firstValue + secondValue;
    }
}
