package Test.problem3;

public interface InterfaceATM {
    public double deposit(double money);
    public double withdraw(double money);
}
