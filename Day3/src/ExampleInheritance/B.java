package ExampleInheritance;

public class B extends A {
    private String nameB = "B";

    public String getNameB() {
        return nameB;
    }
}
