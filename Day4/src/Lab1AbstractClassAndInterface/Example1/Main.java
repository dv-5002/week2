package Lab1AbstractClassAndInterface.Example1;

public class Main {

    public static void main(String[] args) {
        Cat cat=new Cat();
        cat.canMove();
        cat.canEat();

        Fish fish=new Fish();
        fish.canMove();
        fish.canEat();

        Spider spider=new Spider();
        spider.canMove();
        spider.canEat();
    }
}
