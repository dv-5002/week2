package Lab1AbstractClassAndInterface.problem3_5;

public class Rectangle extends Shape implements HeightWidth {
    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public double getTotalLength() {
        return (2 * height) + (2 * width);
    }

    @Override
    public double getArea() {
        return this.height * this.width;
    }

    @Override
    public double getHeight() {
        return this.height;
    }

    @Override
    public double getWidth() {
        return this.width;
    }
}
