package Lab2Encapsulation.problem4;

public class Main {
    public static void main(String args[]) {
        AccountBalance accountBalance=new AccountBalance(1000);
        System.out.println(accountBalance.viewBalance());
        System.out.println(accountBalance.calInterest(1,2));
        System.out.println(accountBalance.deposit(300));
        System.out.println(accountBalance.withdraw(200));
    }
}
