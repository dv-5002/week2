package Lab1AbstractClassAndInterface.problem4;

public class Charmander extends Pokemon {
    @Override
    public void attack(int num) {
        System.out.println("Split fire " + num + " minutes");
    }

    @Override
    public void move(int distance) {
        System.out.println("Run " + (int) Math.ceil(distance / 10.0) + " times");
    }

    @Override
    public void specialMove() {
        double chance = Math.floor(Math.random() * 100);
        if (chance < 50) {
            System.out.println("Success to burn enemy while they move");
        } else {
            System.out.println("Fail to burn enemy while they move");
        }
    }
}
