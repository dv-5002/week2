package Lab1IntroductionToClassAndObject.problem3;

public class GradeCalculation {
  public static void main(String[] args) {
      Student apisit = new Student();
      apisit.score = 70;
      System.out.println(calGrade(apisit));
  }

  public static char calGrade(Student student) {
    if (student.score > 80) {
      return 'A';
    } else if (student.score > 70) {
      return 'B';
    } else if (student.score > 60) {
      return 'C';
    } else {
      return 'F';
    }
  }
}
