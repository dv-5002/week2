package Lab2Encapsulation.problem5;

public class Student {
  private String id;
  private String name;

  public Student(String id) {
    this.id = id;
  }

  void enroll(Course course, int index) {
    course.getStudentGrades()[index] = new StudentGrade(id);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
