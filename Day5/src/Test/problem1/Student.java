package Test.problem1;

public class Student extends Person implements InterfacePerson{
    private String fname;
    private String lname;
    private String ID;
    private int score;

    public Student(String fname, String lname, String ID, int score) {
        this.fname = fname;
        this.lname = lname;
        this.ID = ID;
        this.score = score;
    }

    public void printScore() {
        System.out.println("I received " + score);
    }

    public void printInfo() {
        System.out.println("My name is "+fname+" "+lname+" and I received "+score);
    }
}
