package Lab1AbstractClassAndInterface.problem5;

public class Main {
    public static void main(String[] args) {
        Restaurant restaurant = new Ordinary();
        System.out.println(restaurant.calculateNetPrice(50));

        restaurant = new DriveThrough();
        System.out.println(restaurant.calculateNetPrice(50));

        restaurant = new Airport();
        System.out.println(restaurant.calculateNetPrice(50));
    }
}
