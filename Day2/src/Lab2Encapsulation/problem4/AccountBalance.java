package Lab2Encapsulation.problem4;

public class AccountBalance {
    private double AmountOfMoney;

    public AccountBalance(double deposit) {
        this.AmountOfMoney = deposit;
    }

    public double withdraw(double AmountOfMoney) {
        if (AmountOfMoney > this.AmountOfMoney) {
            System.out.println("Can't withdraw. Don't have enough money.");
        } else {
            this.AmountOfMoney -= AmountOfMoney;
        }
        return this.AmountOfMoney;
    }

    public double deposit(double AmountOfMoney) {
        this.AmountOfMoney += AmountOfMoney;
        return this.AmountOfMoney;
    }

    public double viewBalance() {
        return AmountOfMoney;
    }

    public double calInterest(int numOfYear, double interestRate) {
        return interestRate > 1 ?
                Math.pow(1 + (interestRate / 100), numOfYear) * this.AmountOfMoney :
                Math.pow(1 + interestRate, numOfYear) * this.AmountOfMoney;
    }
}
