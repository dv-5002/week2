package Lab1Inheritance.problem4;

import Lab1Inheritance.problem3.Bicycle;

public class MountainBike extends Bicycle {
    private int gear;

    public MountainBike() {
        this.speed = 0;
        this.gear = 1;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    public void speedUp(double applied) {
        if (speed + 5 <= 99) {
            speed += gear * applied;
            System.out.println("Current speed: " + speed);
        } else {
            System.out.println("Speed cannot exceed 99 km/h");
        }
    }
}
