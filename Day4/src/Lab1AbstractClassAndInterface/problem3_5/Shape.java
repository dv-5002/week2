package Lab1AbstractClassAndInterface.problem3_5;

public abstract class Shape implements Calculate {
    protected double height;
    protected double width;
    protected double radius;
}
