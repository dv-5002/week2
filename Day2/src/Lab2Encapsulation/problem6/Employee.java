package Lab2Encapsulation.problem6;

public class Employee {
  private String id;
  private String name;
  private double monthlySalary;
  private int jobLevel;
  private int amountOfWorkingDays;

  public Employee() {
    this.monthlySalary = 0;
    this.jobLevel = 0;
    this.amountOfWorkingDays = 0;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getMonthlySalary() {
    return monthlySalary;
  }

  public void setMonthlySalary(double monthlySalary) {
    this.monthlySalary = monthlySalary;
  }

  public int getJobLevel() {
    return jobLevel;
  }

  public void setJobLevel(int jobLevel) {
    this.jobLevel = jobLevel;
  }

  public int getAmountOfWorkingDays() {
    return amountOfWorkingDays;
  }

  public void setAmountOfWorkingDays(int amountOfWorkingDays) {
    this.amountOfWorkingDays = amountOfWorkingDays;
  }

  public static double calculateTax(Employee employee) {
    if (employee.getMonthlySalary() <= 100000) {
      return employee.getMonthlySalary() * 0.05;
    } else if (employee.getMonthlySalary() <= 500000) {
      return employee.getMonthlySalary() * 0.1;
    } else {
      return employee.getMonthlySalary() * 0.15;
    }
  }

  public static void improveJobLevel(Employee employee) {
    if (employee.getAmountOfWorkingDays() >= 730) {
      employee.setJobLevel(employee.getJobLevel() + 1);
      employee.setAmountOfWorkingDays(employee.getAmountOfWorkingDays() - 730);
    }
  }
}
