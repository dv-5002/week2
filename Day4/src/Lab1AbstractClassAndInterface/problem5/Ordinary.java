package Lab1AbstractClassAndInterface.problem5;

public class Ordinary extends Restaurant {
    private double discount = 0.1;

    @Override
    public double calculateNetPrice(int price) {
        return price - (price * discount);
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}
