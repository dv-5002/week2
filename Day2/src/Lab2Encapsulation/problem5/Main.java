package Lab2Encapsulation.problem5;

import java.util.ArrayList;

public class Main {
  public static void main(String[] args) {
    Student s1 = new Student("SE001");
    Student s2 = new Student("SE002");
    Student s3 = new Student("SE003");
    Student s4 = new Student("SE004");
    Student s5 = new Student("SE005");
    Student s6 = new Student("SE006");
    Student s7 = new Student("SE007");
    Student s8 = new Student("SE008");
    Student s9 = new Student("SE009");
    Student s10 = new Student("SE010");

    Course oop = new Course();
    oop.setId("953123");
    oop.setName("OOP");
    oop.setCredit(3);
    Course logic = new Course();
    logic.setId("953124");
    logic.setName("Logic");
    logic.setCredit(3);

    s1.enroll(oop, 0);
    s2.enroll(oop, 1);
    s3.enroll(oop, 2);
    s4.enroll(oop, 3);
    s5.enroll(oop, 4);
    s6.enroll(oop, 5);
    s7.enroll(oop, 6);
    s8.enroll(oop, 7);
    s9.enroll(oop, 8);
    s10.enroll(oop, 9);

    s1.enroll(logic, 0);
    s2.enroll(logic, 1);
    s3.enroll(logic, 2);
    s4.enroll(logic, 3);
    s5.enroll(logic, 4);
    s6.enroll(logic, 5);
    s7.enroll(logic, 6);
    s8.enroll(logic, 7);
    s9.enroll(logic, 8);
    s10.enroll(logic, 9);

    oop.getStudentGrades()[0].setGrade("A");
    oop.getStudentGrades()[1].setGrade("B");
    oop.getStudentGrades()[2].setGrade("C");
    oop.getStudentGrades()[3].setGrade("D");
    oop.getStudentGrades()[4].setGrade("F");
    oop.getStudentGrades()[5].setGrade("B+");
    oop.getStudentGrades()[6].setGrade("C+");
    oop.getStudentGrades()[7].setGrade("D+");
    oop.getStudentGrades()[8].setGrade("A");
    oop.getStudentGrades()[9].setGrade("B+");

    logic.getStudentGrades()[0].setGrade("F");
    logic.getStudentGrades()[1].setGrade("D");
    logic.getStudentGrades()[2].setGrade("C");
    logic.getStudentGrades()[3].setGrade("B");
    logic.getStudentGrades()[4].setGrade("A");
    logic.getStudentGrades()[5].setGrade("D+");
    logic.getStudentGrades()[6].setGrade("C+");
    logic.getStudentGrades()[7].setGrade("B+");
    logic.getStudentGrades()[8].setGrade("A");
    logic.getStudentGrades()[9].setGrade("F");

    ArrayList<Course> courses = new ArrayList();
    courses.add(oop);
    courses.add(logic);

    double average = GradeCalculator.calculateAverage(courses);

    System.out.println(average);
  }
}
