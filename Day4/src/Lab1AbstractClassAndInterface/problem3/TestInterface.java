package Lab1AbstractClassAndInterface.problem3;

public class TestInterface implements Example1, Example2 {

    @Override
    public void testMethod() {
        System.out.println("This is method from Example" + "1");
    }

    @Override
    public void testMethod2() {
        System.out.println("This is method from Example" + "2");
    }
}
