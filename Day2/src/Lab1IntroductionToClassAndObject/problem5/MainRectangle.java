package Lab1IntroductionToClassAndObject.problem5;

import java.util.Arrays;

public class MainRectangle {
  public static void main(String[] args) {
    Rectangle rectangle = new Rectangle(0, 0, 10, 5);
    System.out.println("The area = "+rectangle.calArea());

    for (double[] coordinate: rectangle.calCoordinates()){
      System.out.println(Arrays.toString(coordinate));
    }
  }
}