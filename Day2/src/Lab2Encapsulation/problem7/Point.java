package Lab2Encapsulation.problem7;

public class Point {
  private double xCoordinate;
  private double yCoordinate;

  public Point() {
    this.xCoordinate = 0;
    this.yCoordinate = 0;
  }

  public Point(double coordinate) {
    this.xCoordinate = coordinate;
    this.yCoordinate = coordinate;
  }

  public Point(double xCoordinate, double yCoordinate) {
    this.xCoordinate = xCoordinate;
    this.yCoordinate = yCoordinate;
  }

  public boolean equal(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Point point = (Point) o;
    return Double.compare(point.xCoordinate, xCoordinate) == 0 &&
        Double.compare(point.yCoordinate, yCoordinate) == 0;
  }

  public double getXCoordinate() {
    return xCoordinate;
  }

  public void setXCoordinate(double xCoordinate) {
    this.xCoordinate = xCoordinate;
  }

  public double getYCoordinate() {
    return yCoordinate;
  }

  public void setYCoordinate(double yCoordinate) {
    this.yCoordinate = yCoordinate;
  }

  public String print() {
    return String.format("Point(%f,%f)",xCoordinate,yCoordinate);
  }
}
