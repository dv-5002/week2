package Lab2Polymorphism.problem3;

public class MathObject {
    int firstValue;
    int secondValue;

    public MathObject(int firstValue, int secondValue) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;
    }

    public int compute() {
        return 0;
    }
}
