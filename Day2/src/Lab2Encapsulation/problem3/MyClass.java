package Lab2Encapsulation.problem3;

public class MyClass {
    protected MyClass(){
        System.out.println("I am a no argument constructor");
        System.out.println("I Can have additional logic");
    }
}
