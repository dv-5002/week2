package Lab1IntroductionToClassAndObject.problem4;

public class MainCar {
    public static void main(String[] args) {
        Car black=new Car();
        black.speed=60.0;
        black.brake(10.0);
        System.out.println("Car brake speed = "+black.speed);
        black.accelerate(15.2);
        System.out.println("Car accelerate speed = "+black.speed);
    }
}
