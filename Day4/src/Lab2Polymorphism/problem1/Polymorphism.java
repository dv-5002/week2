package Lab2Polymorphism.problem1;

public class Polymorphism {
    public static void main(String args[]) {
        Multiply m = new Multiply();
        m.mul(6, 10);
        m.mul(10, 6, 5);
        m.mul(5.5, 6.5);
    }
}
