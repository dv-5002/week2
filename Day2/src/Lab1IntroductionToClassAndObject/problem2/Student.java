package Lab1IntroductionToClassAndObject.problem2;

public class Student {
    String name;
    String studentID;
    int score;

    int compareScore(Student otherStudent) {
        return otherStudent.score - this.score;
    }
}
