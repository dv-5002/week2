package Lab1AbstractClassAndInterface.problem4;

public abstract class Pokemon {
    public abstract void attack(int num);

    public abstract void move(int distance);

    public abstract void specialMove();
}
