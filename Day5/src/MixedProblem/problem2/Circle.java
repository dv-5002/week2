package MixedProblem.problem2;

public class Circle extends Shape implements InterfaceShape{
    private String name = "Circle";

    private double radius;

    public Circle() {
        this(0.0);
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double calArea() {
        return Math.PI * Math.pow(radius, 2.0);
    }

    public double calCircumference() {
        return 2.0 * Math.PI * radius;
    }
}
