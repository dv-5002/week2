package Lab1AbstractClassAndInterface.problem2;

public class Main {
    public static void main(String args[]){
        Guitar g = new Guitar();
        g.play();

        Bass b = new Bass();
        b.play();
    }
}
